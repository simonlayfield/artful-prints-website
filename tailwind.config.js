/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    extend: {
      colors: {
        purple: {
          "100": "#e7d4ff",
          "200": "#d0aaff",
          "300": "#a862ff",
          "400": "#8a2aff",
          "500": "#7200ff",
          "600": "#6300dd",
          "700": "#5500bd",
          "800": "#4800a1",
          "900": "#340074"
        }
      },
      inset: {
        "-1": "-1rem",
        "1/2": "calc(50% - 7rem)"
      }
    }
  },
  variants: {
    borderRadius: ["first", "last"],
    appearance: ["responsive", "hover", "focus"]
  },
  plugins: []
};
