module.exports = {
  mode: "universal",
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        type: "text/css",
        href:
          "https://fonts.googleapis.com/css2?family=Assistant:wght@400;700;800&display=swap"
      },
      {
        rel: "stylesheet",
        type: "text/css",
        href: "https://cdn.snipcart.com/themes/v3.0.12/default/snipcart.css"
      }
    ],
    script: [
      { src: "https://cdn.snipcart.com/themes/v3.0.12/default/snipcart.js" }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: ["@/assets/css/main.css"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    "@nuxtjs/tailwindcss"
  ],
  /*
   ** Nuxt.js modules
   */
  modules: ["nuxt-svg-loader"],
  purgeCSS: {
    paths: ["assets/svg/*.svg"],
    whitelistPatterns: [/fill/, /grid-cols-/],
    whitelist: [
      "annequin",
      "parlez",
      "dorian",
      "jordan",
      "appelby",
      "tank",
      "rippin",
      "wish",
      "palentino",
      "avanti",
      "loco",
      "josie"
    ]
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    extractCSS: true
  },
  env: {
    baseURL:
      process.env.NODE_ENV === "production"
        ? "https://artful-prints.now.sh"
        : "http://localhost:3000"
  }
};
